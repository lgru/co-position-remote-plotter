var activePath;
var plotBounds = null;
var line = new Line ();
var circle = new Circle ();
var polyline = new Polyline ();
var rectangle = new Rectangle ();
var input = new Input ();
var device = null;
var pen = 0;
var speed = 10;
var commandBuffer = new Array;
var canvasCommands = new Array;
var canvasBounds = null;
var activeSession = null;
var penColors = ['#00ffff', '#ff00ff', '#ffff00', '#00000', '#ffff5b']
var activeColor = penColors[0]

function Line () {
	this.activePath = new Path ()
	
	this.onMouseDown=function (event) {
		this.activePath = new Path();
		this.activePath.strokeColor = activeColor;
	} 
	
	this.onMouseDrag=function (event) { 
		this.activePath.add(event.point);
	} 
	
	this.onMouseUp=function (event) {
		this.activePath.simplify ()
		this.activePath.flatten (4)
		
		var points = new Array;
		for (i=0;i<this.activePath.segments.length;i++) {
			point = transformPoint (this.activePath.segments[i].point)
			points.push (point.x + ',' + point.y)
		}
		
		command = 'PA' + points.shift() + ';PD' + points.join (',') + ';'
		
		sendCommand (command)
	}
	
	this.onMouseMove=function (event) {
	}
	
	this.onKeyDown=function (event) {
	}
	
	this.onKeyUp=function (event) {
	}
}

function Polyline () {
	this.activePath = new Path ();
	
	this.onMouseDown=function (event) {
		if (this.activePath.segments.length == 0) {
			this.activePath = new Path ();
			this.activePath.strokeColor = activeColor;
			this.activePath.add(event.point);
		}
		
		this.activePath.add(event.point);
	} 
	
	this.onMouseDrag=function (event) {
		this.activePath.lastSegment.point = event.point
	} 
	
	this.onMouseUp=function (event) {
		this.activePath.lastSegment.point = event.point
	}
	
	this.onMouseMove=function (event) {
		if (this.activePath.segments.length > 0) {
			this.activePath.lastSegment.point = event.point
		}
	}
	
	this.onKeyDown=function (event) {
		if (event.key == 'enter') {
			var points = new Array;
			
			this.activePath.removeSegment (this.activePath.segments.length-1)
			
			for (i=0;i<this.activePath.segments.length;i++) {
				var point = transformPoint (this.activePath.segments[i].point);
				points.push (point.x + ',' + point.y);
			}
			
			if (points.length > 1) {
				command = 'PA' + points.shift() + ';PD' + points.join (',') + ';';
			} else {
				command = 'PA' + points.shift() + ';PD;';
			}
			sendCommand (command);
			
			this.activePath = new Path();
		}

	}
	
	this.onKeyUp=function (event) {
	}
}


function Circle () {
	this.point = new Point(0,0)
	this.lastpoint = new Point(0,0)
	this.origin = new Point (0,0)
	this.radius = 0
	this.circle = new Path.Circle (this.origin,this.radius)
	this.event = null
	
	this.setRadius=function () {
		radius = Math.sqrt ((Math.pow (this.point.x - this.lastpoint.x, 2) + Math.pow (this.point.y - this.lastpoint.y, 2)));
		this.radius = (this.event.modifiers.control) ? radius : radius * 0.5
	}
	
	this.setOrigin=function (origin) {
		if (this.event.modifiers.control) {
			var origin = new Point (this.point.x, this.point.y);
		} else {
			distance = Math.sqrt (Math.pow (this.radius, 2) / 2);
			
			var x = (this.lastpoint.x - this.point.x < 0) ? this.point.x - distance : this.point.x + distance;
			var y = (this.lastpoint.y - this.point.y < 0) ? this.point.y - distance : this.point.y + distance;
			
			var origin = new Point (x,y)		
		}
		
		this.origin = origin;
	}
	
	this.draw = function () {
		this.circle.remove();
		this.circle = new Path.Circle (this.origin, this.radius)
		this.circle.strokeColor = activeColor;
	}
	
	this.detach = function () {
		this.point = new Point (0,0)
		this.lastpoint = new Point (0,0)
		this.origin = new Point (0,0)
		this.radius = 0
		this.circle = new Path.Circle (this.origin, this.radius)
		this.circle.strokeColor = activeColor;
	}
	
	this.onMouseDown=function (event) {
		this.event = event;
		this.point = event.point;
		this.lastpoint = event.point;
		this.setRadius ();
		this.setOrigin ();
		this.draw ()
	} 
	
	this.onMouseDrag=function (event) {
		this.lastpoint = event.point;
		this.setRadius ();
		this.setOrigin ();
		this.draw ()
		
	} 
	
	this.onMouseUp=function (event) {
		this.lastpoint = event.point;
		this.setRadius ();
		this.setOrigin ();
		this.draw ()
		
		var origin = transformPoint (this.origin)
		plotradius = convertToPlotterPoints (this.radius)
		
		command = 'PA' + origin.x + ',' + origin.y + ';CI' + plotradius + ';'
		
		sendCommand (command)
		this.detach ();
	}
	
	this.onMouseMove=function (event) {
	}
	
	this.onKeyDown=function (event) {
		this.event = event;
		this.setRadius ();
		this.setOrigin ();
		this.draw ()
	}
	
	this.onKeyUp=function (event) {
		this.event = event;
		this.setRadius ();
		this.setOrigin ();
		this.draw ()
	}
}

function Rectangle () {
	this.point = new Point(0,0)
	this.lastpoint = new Point(0,0)
	this.size = [0,0]
	this.origin = [0,0]
	this.rectangle = new Path.Rectangle (this.size, this.origin)
	this.event = null
	
	this.setSize=function () {
		var dx = Math.abs (this.lastpoint.x - this.point.x);
		var dy = Math.abs (this.lastpoint.y - this.point.y);
		
		if (this.event.modifiers.shift) {
			size = (dx > dy) ? [dx, dx] : [dy, dy];
		} else {
			size = [dx, dy]
		}
		
		this.size = (this.event.modifiers.control) ? [size[0] * 2, size[1] * 2] : size;
	}
	
	this.setOrigin=function () {
		if (this.event.modifiers.control) {
			origin = new Point (this.point.x - this.size[0] * 0.5, this.point.y - this.size[1] * 0.5);
		} else {
			origin = new Point (this.point.x, this.point.y);
			
			var dx = this.lastpoint.x - this.point.x;
			var dy = this.lastpoint.y - this.point.y;
			
			if (dx < 0)
				origin.x = origin.x - size[0];
			if (dy < 0)
				origin.y = origin.y - size[1];
		}
		
		this.origin = origin;
	}
	
	this.draw = function () {
		this.rectangle.remove();
		this.rectangle = new Path.Rectangle (this.origin, this.size)
		this.rectangle.strokeColor = activeColor;
	}
	
	this.detach = function () {
		this.point = new Point(0,0)
		this.lastpoint = new Point(0,0)
		this.size = [0,0]
		this.origin = [0,0]
		this.rectangle = new Path.Rectangle (this.size, this.origin)
		this.event = null
	}
	
	this.onMouseDown=function (event) {
		this.event = event;
		this.point = event.point;
		this.lastpoint = event.point;
		this.setSize();
		this.setOrigin();
		this.draw();
	} 
	
	this.onMouseDrag=function (event) {
		this.event = event;
		this.lastpoint = event.point;
		this.setSize();
		this.setOrigin();
		this.draw();
	} 
	
	this.onMouseUp=function (event) {
		this.event = event;
		this.lastpoint = event.point;
		this.setSize();
		this.setOrigin();
		this.draw();
		
		origin = transformPoint (this.origin);
		width = convertToPlotterPoints (this.size[0]).toFixed(2);
		height = convertToPlotterPoints (this.size[1]).toFixed(2);
 		
		command = 'PA' + origin.x + ',' + origin.y + ';PR;PD0,' + width + ',' + height + ',0,0,' + width * -1 + ',' + height * -1 + ',0;' 
 		
		sendCommand (command)
		
		this.detach();
	}
	
	this.onMouseMove=function (event) {
	}
	
	this.onKeyDown=function (event) {
		this.event = event;
		this.setSize();
		this.setOrigin();
		this.draw();
	}
	
	this.onKeyUp=function (event) {
		this.event = event;
		this.setSize();
		this.setOrigin();
		this.draw();
	}
}

function Input () {
	
	this.setSize=function () {
		
	}
	
	this.setOrigin=function () {
	}
	
	this.draw = function () {
	}
	
	this.detach = function () {
	}
	
	this.onMouseDown=function (event) {
	} 
	
	this.onMouseDrag=function (event) {
	} 
	
	this.onMouseUp=function (event) {
	}
	
	this.onMouseMove=function (event) {
	}
	
	this.onKeyDown=function (event) {
	}
	
	this.onKeyUp=function (event) {
	}
	
	this.getInput=function () {
		command = window.prompt ('?')
		sendBareCommand (command)
	}
}


function drawHPGL () {
	this.point = new Point (0,0);
	this.penDown = 0;
	this.penUp = 1;
	this.penState = this.penUp;
	this.noPen = 0;
	this.pen = this.noPen;
	this.coordAbsolute = 1;
	this.coordRelative = 0;
	this.coordSystem = this.coordAbsolute;
	this.color = 'Black';
	this.path = null;
	this.knownCommands = ['PD','PU','PA','PR','CI','SP']
	this.commandMatch = /[a-z]+/i
	this.argumentMatch = /[0-9\.,-]+/
	this.commandSplit = ';'
	this.argumentSplit = ','
	
	this.SP = function () {
		if (arguments.length > 0) {
			this.pen = arguments[0];
			this.color = penColors[this.pen - 1]
		} else {
			this.pen = this.noPen;
		}
	}
	
	this.CI = function (r) {
		this.drawCircle (r)
	}
	
	this.PR = function () {
		this.coordSystem = this.coordRelative;
	}
	
	this.PA = function () {
		this.coordSystem = this.coordAbsolute;
		
		if (arguments.length > 1) {
			for (var i=0;i < arguments.length; i=i+2) {
				this.moveTo (arguments[i], arguments[i+1]);
			}
		}
	}
	
	this.PD = function () {
		this.penState = this.penDown;
		this.createPath ();
		
		for (var i=0;i < arguments.length; i=i+2) {
			this.moveTo (arguments[i], arguments[i+1]);
		}
	}
	
	this.PU = function () {
		this.penState = this.penUp;
		this.closePath ()
	}
	
	this.createPath = function () {
		this.path = new Path ();
		this.path.add (transformCoords (this.point))
		this.path.strokeColor = this.color;
	}
	
	this.closePath = function () {
		this.path = null;
	}
	
	this.moveTo = function () {
		this.point = (this.coordSystem == this.coordAbsolute) ? new Point (arguments[0], arguments[1]) : this.point + new Point (arguments[0], arguments[1]);
		
		if (this.penState == this.penDown) {
			this.lineTo (this.point);
		}
	}
	
	this.lineTo = function (point) {
		if (this.pen != this.noPen) {
			this.path.add (transformCoords (point));
		}
	}
	
	this.drawCircle = function (radius) {
		if (this.pen != this.noPen) {
			c = new Path.Circle (transformCoords (this.point), convertToCanvasPoints (radius));
		}
		c.strokeColor = this.color;
	}
	
	this.parse = function () {
		if (arguments != undefined) {
			for (var i=0;i < arguments.length; i++) {
				commands = arguments[i].split (this.commandSplit);
				for (var c=0; c < commands.length; c++) {
					if (commands[c].length > 0) {
						command = commands[c].match (this.commandMatch)[0].toUpperCase()
						args = commands[c].match (this.argumentMatch)
						
						if (args != null) {
							args = args[0].split (this.argumentSplit)
							
							for (var a = 0; a < args.length; a++) {
								args[a] = parseFloat (args[a])
							}
						} else {
							args = []
						}
						
						if (this.knownCommands.indexOf (command) > -1) {
							this[command].apply(this, args)
						}
					}
				}
			}
		}
	}
}

$(document).ready (function () {
	
	$(window).resize(function() {
		scaleCanvas();
	});
	
	showMessage ('Starting up')
	
	scaleCanvas();
	
	startPlotter ();
	
	device = line;
	
	pen = $('#penSelect').val();
	
	$('#lineTool').click (function () {device = line;$('.active_tool').toggleClass ('active_tool');$(this).toggleClass('active_tool')})
	$('#circleTool').click (function () {device = circle;$('.active_tool').toggleClass ('active_tool');$(this).toggleClass('active_tool')})
	$('#polylineTool').click (function () {device = polyline;$('.active_tool').toggleClass ('active_tool');$(this).toggleClass('active_tool')})
	$('#rectangleTool').click (function () {device = rectangle;$('.active_tool').toggleClass ('active_tool');$(this).toggleClass('active_tool')})
	$('#inputTool').click (function () {device = input;$('.active_tool').toggleClass ('active_tool');$(this).toggleClass('active_tool');device.getInput()})
	$('#penSelect').change (function () {
		pen = $(this).val();
		activeColor = penColors[pen - 1]
	});
	
	$('#penSpeed').change (function () {
		speed = $(this).val();
	});
	
	sendCommandToServer ();

})

function scaleCanvas () {
	height = $(window).height() - 50;
	width = height / Math.sqrt(2);
	view.viewSize = new Size (width, height)
	canvasBounds = [[view.bounds.left,view.bounds.top], [view.bounds.right, view.bounds.bottom]]
}

function startPlotter () {
	$.get ('/instantiate', function (data) {
		data = $.parseJSON (data);
		project.activeLayer.remove();
		var layer = new Layer();
		plotBounds = [[data['bottom'], data['left']], [data['top'], data['right']]];
		activeSession = data.session
		canvasCommands = []
		plotterReady ();
	})	
}

function instantiateSession () {
	$.get ('/instantiate', function (data) {
		data = $.parseJSON (data);
		project.activeLayer.remove();
		var layer = new Layer();
		plotBounds = [[data['bottom'], data['left']], [data['top'], data['right']]];
		activeSession = data.session
		canvasCommands = []
	})
}



function getSessionCommands () {
	$.get ('/get_commands', function (data) {
		var data = $.parseJSON (data)
		if (data.session != activeSession) {
			instantiateSession ()
		}
		hpgl = new drawHPGL ()
		for (var i=0;i<data.commands.length;i++) {
			if (canvasCommands.indexOf (data.commands[i].id) < 0) {
				hpgl.parse (data.commands[i].command)
				canvasCommands.push (data.commands[i].id)
			}
		}
		
		view.draw();
	})
	
	setTimeout (function () {getSessionCommands()}, 2000)
}

function plotterReady () {
	showMessage ('We\'re good to go!')
	getSessionCommands ();
}

function showMessage (text) {
	$('#messageBox span').html (text);
}

function onMouseDown(event) {
	device.onMouseDown (event);
}

function onMouseDrag(event) {
	device.onMouseDrag (event);
}

function onMouseUp(event) {
	device.onMouseUp (event);
}

function onMouseMove(event) {
	device.onMouseMove (event);
}

function onKeyDown(event) {
	device.onKeyDown (event);
}

function onKeyUp(event) {
	device.onKeyUp (event);
}

function transformPoint (point) {
	y = (point.x - canvasBounds[0][0]) * ((plotBounds[1][0] - plotBounds[0][0]) / (canvasBounds[1][0] - canvasBounds[0][0])) + plotBounds[0][0]
	x = (point.y - canvasBounds[0][1]) * ((plotBounds[1][1] - plotBounds[0][1]) / (canvasBounds[1][1] - canvasBounds[0][1])) + plotBounds[0][1]
	
	return new Point (parseFloat (x.toFixed (2)), parseFloat (y.toFixed(2)))
}

function transformCoords (point) {
	x = (point.y - plotBounds[0][0]) * ((canvasBounds[1][0] - canvasBounds[0][0]) / (plotBounds[1][0] - plotBounds[0][0])) + canvasBounds[0][0]
	y = (point.x - plotBounds[0][1]) * ((canvasBounds[1][1] - canvasBounds[0][1]) / (plotBounds[1][1] - plotBounds[0][1])) + canvasBounds[0][1]
	
	return new Point (parseFloat (x.toFixed(2)), parseFloat (y.toFixed (2)))
}

function convertToPlotterPoints (length) {
	return length * (plotBounds[1][0] - plotBounds[0][0]) / (canvasBounds[1][0] - canvasBounds[0][0]);
}

function convertToCanvasPoints (length) {
	return length * (canvasBounds[1][0] - canvasBounds[0][0]) / (plotBounds[1][0] - plotBounds[0][0]);
}

function sendCommand (command) {
	commandBuffer.push ('SP' + pen + ';VS' + speed + ';' + command + ';PU');
}

function sendBareCommand (command) {
	commandBuffer.push (command);
}

function sendCommandToServer () {
	if (commandBuffer.length > 0) {
		$.post('/command', {'command': commandBuffer.join (';')}, function (data) {
			var data = $.parseJSON (data);
			if (data.code == 100) {
				canvasCommands.push (data.id);
			}
		});
		
		commandBuffer = new Array;
	}
	
	setTimeout (function () { sendCommandToServer (); }, 2000);
}