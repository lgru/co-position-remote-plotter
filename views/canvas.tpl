<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript" src="/js/paper.js"></script>
		<script type="text/javascript" src="/js/jquery.js"></script>
		<script type="text/paperscript" canvas="canvas" src="/js/draw.js"></script>
	</head>
	<style type="text/css">
		body, html {
			margin: 0;
			padding: 0;
			background: #efefef;
		}
		
		canvas {
			margin: 25px 0px 25px 25px;
			background: #ffffff;
		}
		
		#canvas {
			float: left;
		}
		
		#messageBox {
			font-family: sans-serif;
			font-size: 11px;
			text-transform: uppercase;
			color: #999999;
			text-align: center;
			margin-bottom: 10px;
		}
		
		.tool {
			margin: 0px 0px 10px 0px;
			width: 80px;
			border: 1px solid #999999;
			font-family: sans-serif;
			font-size: 11px;
			text-transform: uppercase;
			color: #99999;
			text-align: center;
			padding-top: 6px;
			padding-bottom: 6px;
		}
		
		.active_tool {
			background-color: #ffffff;
		}
		
		#toolbox {
			margin: 25px 0px 25px 25px;
			float: left;
		}
	</style>
	<body>
		<canvas id="canvas"></canvas>
		<div id="toolbox">
			<div id="messageBox"><span></span></div>
			<div id="speedTool" class="tool"><input type="text" name="penSpeed" id="penSpeed" size="3" value="10" /></div>
			<div id="penTool" class="tool">
				<select id="penSelect">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
				</select>
			</div>
			<div id="lineTool" class="tool active_tool">Line</div>
			<div id="polylineTool" class="tool">Polyline</div>
			<div id="circleTool" class="tool">Circle</div>
			<div id="rectangleTool" class="tool">Rectangle</div>
			<div id="inputTool" class="tool">Input</div>
		</div>
	</body>
</html>