from bottle import view, route, run, route, request, install, template, response, static_file
from sqlite3 import IntegrityError, Error
from bottle_sqlite import SQLitePlugin
from json import dumps
from chiplotle import *
from chiplotle.tools.plottertools import instantiate_virtual_plotter
from sys import argv

install (SQLitePlugin(dbfile='plotter.db'))
VIRTUAL_PLOTTER = 0
HARDWARE_PLOTTER = 1

BOTTLE_SERVER = 0
CHERRYPY_SERVER = 1

# python app.py plotter_type host port server

# Defines whether we use Chiplotle's virtual plotter - useful for testing - 
# or the hardware plotter. It needs to be connected to instantiate a session!
PLOTTER_TYPE = HARDWARE_PLOTTER if argv[1].lower() == 'hardware' else VIRTUAL_PLOTTER

# The host for the server
HOST = argv[2]
# Used port
PORT = int (argv[3])
# Used server. Cherrpy is advised in production, but needs to be installed
# Otherwise user bottle's build-in server, but it might get slow.
SERVER = CHERRYPY_SERVER if argv[4].lower() == 'cherrypy' else BOTTLE_SERVER
	
@route('/')
@route('/index')
def index (db):
    return template('canvas')

@route('/command', method='POST')
def command(db):
	command = request.forms.command
	c = db.cursor();
	
	try:
		c.execute("INSERT INTO commands (command,session) VALUES(:command, (SELECT id FROM sessions WHERE active = 1))", {'command': command})
		db.commit ()
	except:
		return dumps ({'command': command, 'code': 200})
	
	return dumps ({'command': command, 'code': 100, 'id': c.lastrowid})

@route('/instantiate')
def instantiate (db):
	try:
		session = db.execute("SELECT * FROM sessions WHERE active = 1").fetchone()
		
	except:
		return dumps ({'code': 200})
	
	return dumps ({'top': session['top'], 'bottom': session['bottom'], 'left': session ['left'], 'right': session['right'], 'session': session['id']})

@route('/active_session')
def active_session(db):
	session = db.execute("SELECT * FROM sessions WHERE active = 1").fetchone()
	return dumps ({'session': session['id']})
	
@route('/new_session')
def new_session(db):
	if PLOTTER_TYPE == HARDWARE_PLOTTER:
		plotter = instantiate_plotters()[0]
	else:
		plotter = instantiate_virtual_plotter (type='HP7576A')
	
	margins = plotter.margins.soft
	left = margins.left
	right = margins.right
	top = margins.top
	bottom = margins.bottom
	
	try:
		db.execute("UPDATE sessions SET active = 0 WHERE active = 1")
		db.execute("INSERT INTO sessions (active, top, right, bottom, left) VALUES(1, :top, :right, :bottom, :left)", {'top': top, 'right': right, 'bottom': bottom, 'left': left})
		db.commit ()
	except:
		return dumps ({'code': 200})
	
	return dumps ({'code': 100})

@route('/get_commands')
@route('/get_commands/<session:int>')
def get_commands(db, session=False):
	commands = []
	
	if session == False:
		session = db.execute("SELECT id FROM sessions WHERE active = 1").fetchone()['id']
		for row in db.execute ('SELECT c.command, c.id FROM commands c JOIN sessions s ON s.id = c.session WHERE s.active = 1 ORDER BY c.timestamp ASC').fetchall():
			commands.append ({'command': row['command'], 'id': row['id']})
	else:
		for row in db.execute ('SELECT c.command, c.id FROM commands c WHERE c.session = :session ORDER BY c.timestamp ASC', {'session': session}).fetchall():
			commands.append ({'command': row['command'], 'id': row['id']})

	
	return dumps ({'code': 100, 'commands': commands, 'session': session})

@route('/js/<filename>')
def js (filename):
    return static_file (filename, root='js')
	
#run (host="192.168.1.391", port=8080)
if SERVER == BOTTLE_SERVER:
	run (host=HOST, port=PORT)
if SERVER == CHERRYPY_SERVER:
	run (server='cherrypy', host=HOST, port=PORT)